# Typescript Express WSS Dashboard API

The purpose of this application is to serve as a RESTful API and Web Socket Server for the React Analytics dashboard.

### Dependencies

- `npm install` - install the app

```sh
> npm install -g typescript
```

- `tsc -w` - this application expect the Typescript complier to be installed globally

```sh
> npm install -g typescript
```

- `nodemon` - this application expects nodemon to be be runnable from `package.json`

```sh
> npm install -g nodemon
```

## Run

```sh
npm run dev
```

## Services

JWT is handled as a middleware